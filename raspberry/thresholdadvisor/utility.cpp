#include "utility.h"

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

using namespace std;
using namespace cv;

/* Lettura della libreria dei volti */
void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, std::map<int, string>& labelsInfo, char separator) {
	ifstream csv(filename.c_str());
	if (!csv) CV_Error(CV_StsBadArg, "No valid input file was given, please check the given filename.");
	string line, path, classlabel, info;

	// flag per controllo dimensioni immagine
	bool showSmallSizeWarning = true;

	//int *nPictureById; 		//numero di foto per ogni persona

	while (getline(csv, line)) {
		stringstream liness(line);

		path.clear(); classlabel.clear(); info.clear();

		getline(liness, path, separator);
		getline(liness, classlabel, separator);
		getline(liness, info, separator);

		if (!path.empty() && !classlabel.empty()) {
			//cout << "Processing " << path << endl;
			int label = atoi(classlabel.c_str());
			if (!info.empty())
				labelsInfo.insert(std::make_pair(label, info));
			// 'path' can be file, dir or wildcard path
			String root(path.c_str());
			vector<String> files;
			glob(root, files, true);
			int w = -1, h = -1;
			for (vector<String>::const_iterator f = files.begin(); f != files.end(); ++f) {
				//cout << "\t" << *f << endl;
				Mat img = imread(*f, CV_LOAD_IMAGE_GRAYSCALE);
				if (w > 0 && h > 0 && (w != img.cols || h != img.rows)) cout << "\t* Warning: images should be of the same size!" << endl;
				if (showSmallSizeWarning && (img.cols < 50 || img.rows < 50)) {
					cout << "* Warning: for better results images should be not smaller than 50x50!" << endl;
					showSmallSizeWarning = false;
				}
				// setto per il prossimo confronto
				w = img.cols;
				h = img.rows;
				images.push_back(img);
				labels.push_back(label);
				//nPictureById[label]++;
			}
		}
	}

	//printf("[Init] %d pictures read to train\n", images.size());

	//for (int j = 0; j < labelsInfo.size(); j++)
	//{
	//printf("[Init] %d pictures of %s (%d) read to train\n", nPictureById[j], labelsInfo.find(j)->second.c_str(), j);
	//}
}