#include "utility.h"

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

// sudo apt-get install libcurl4-gnutls-dev e modifiche al CMakeList
#include <curl/curl.h>

//Current timestamp
#include <ctime>

using namespace std;
using namespace cv;

/* Lettura della libreria dei volti */
void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, std::map<int, string>& labelsInfo, char separator) {
	ifstream csv(filename.c_str());
	if (!csv) CV_Error(CV_StsBadArg, "No valid input file was given, please check the given filename.");
	string line, path, classlabel, info;

	// flag per controllo dimensioni immagine
	bool showSmallSizeWarning = true;

	//int *nPictureById; 		//numero di foto per ogni persona

	while (getline(csv, line)) {
		stringstream liness(line);

		path.clear(); classlabel.clear(); info.clear();

		getline(liness, path, separator);
		getline(liness, classlabel, separator);
		getline(liness, info, separator);

		if (!path.empty() && !classlabel.empty()) {
			//cout << "Processing " << path << endl;
			int label = atoi(classlabel.c_str());
			if (!info.empty())
				labelsInfo.insert(std::make_pair(label, info));
			// 'path' can be file, dir or wildcard path
			String root(path.c_str());
			vector<String> files;
			glob(root, files, true);
			int w = -1, h = -1;
			for (vector<String>::const_iterator f = files.begin(); f != files.end(); ++f) {
				//cout << "\t" << *f << endl;
				Mat img = imread(*f, CV_LOAD_IMAGE_GRAYSCALE);
				if (w > 0 && h > 0 && (w != img.cols || h != img.rows)) cout << "\t* Warning: images should be of the same size!" << endl;
				if (showSmallSizeWarning && (img.cols < 50 || img.rows < 50)) {
					cout << "* Warning: for better results images should be not smaller than 50x50!" << endl;
					showSmallSizeWarning = false;
				}
				// setto per il prossimo confronto
				w = img.cols;
				h = img.rows;
				images.push_back(img);
				labels.push_back(label);
				//nPictureById[label]++;
			}
		}
	}

	//printf("[Init] %d pictures read to train\n", images.size());

	//for (int j = 0; j < labelsInfo.size(); j++)
	//{
	//printf("[Init] %d pictures of %s (%d) read to train\n", nPictureById[j], labelsInfo.find(j)->second.c_str(), j);
	//}
}

/* Get current date/time, format is YYYY-MM-DD HH:mm:ss */
std::string currentDateTime() {
	time_t rawtime;
	struct tm * localTime;
	char buffer[80];

	time(&rawtime);

	//trasforma il time raw nel locale corrente
	localTime = localtime(&rawtime);

	/* 	int Day = localTime->tm_mday;
		int Month = localTime->tm_mon + 1;
		int Year = localTime->tm_year + 1900;
		int Hour = localTime->tm_hour;
		int Min = localTime->tm_min;
		int Sec = localTime->tm_sec;

		std::cerr << "This program was exectued at: " << Hour << ":" << Min << ":" << Sec << std::endl;
		std::cerr << "And the current date is: " << Day << "/" << Month << "/" << Year << std::endl; */

	strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localTime);

	return std::string(buffer); //it converts into std::string automatically!
}

/* Get RaspberryPi Unique Serial Number from /proc/cpuinfo */
std::string getSerialNumber() {
	FILE *f = fopen("/proc/cpuinfo", "r");
	if (!f) {
		return NULL;
	}

	char line[256];
	char serial_string[16 + 1];
	while (fgets(line, 256, f)) {
		if (strncmp(line, "Serial", 6) == 0) {
			strcpy(serial_string, strchr(line, ':') + 2);
			break;
		}
	}

	// Rimuoviamo il new line
	serial_string[strcspn(serial_string, "\n")] = 0;

	fclose(f);

	return serial_string;
}

void *send_segnalazione(void *threadarg) {
	SEG_PARAMS* params = (SEG_PARAMS*)threadarg;

	CURL *curl;
	CURLcode res;

	struct curl_httppost *formpost = NULL;
	struct curl_httppost *lastptr = NULL;

	curl_global_init(CURL_GLOBAL_ALL);

	std::ostringstream jsonData, serverUrl;

	//{"Data":"Jul 5, 2015 8:49:00 PM","IdDispositivo":"1674561d95d71c27","Latitudine":0.0,"Longitudine":0.0,"Tipo":1}
	
	/* Costruzione del json della segnalazione */
	jsonData << "{";
	jsonData << "\"Data\":\"" << *(params->dateTime) << "\",";
	jsonData << "\"IdDispositivo\":\"" << *(params->raspberryUUID) << "\",";
	
	// Posizione codificata -> via Bornaccino, Santarcangelo
	jsonData << "\"Latitudine\":44.0583077,";
	jsonData << "\"Longitudine\":12.465183099,";
	
	jsonData << "\"Tipo\":7";		//tipo 7/8 -> Segnalazione manuale
	jsonData << "}";
	
	

	/* Allego json */
	curl_formadd(&formpost,
		&lastptr,
		CURLFORM_COPYNAME, "Segnalazione",
		CURLFORM_CONTENTTYPE, "text/plain",
		CURLFORM_COPYCONTENTS, jsonData.str().c_str(),
		CURLFORM_END);

	/* Allego il frame */

	/* Conversione del vector<uchar> in un buffer classico char * */
	std::vector<uchar> data = *(params->imgData);
	char * buffer = reinterpret_cast<char*> (&data[0]);

	curl_formadd(&formpost,
		&lastptr,
		CURLFORM_COPYNAME, "image",
		CURLFORM_CONTENTTYPE, "image/jpeg",
		CURLFORM_BUFFER, "sc_tmp.jpg",
		CURLFORM_BUFFERPTR, buffer,
		CURLFORM_BUFFERLENGTH, data.size(),
		CURLFORM_END);

	curl = curl_easy_init();

	if (curl) {
		serverUrl << "http://";
		serverUrl << *(params->serverAddress);
		serverUrl << serverEndPoint;

		//printf("Endpoint: %s", serverUrl.str().c_str() );

		/* what URL that receives this POST */
		curl_easy_setopt(curl, CURLOPT_URL, serverUrl.str().c_str());
		curl_easy_setopt(curl, CURLOPT_POST, 1); //set mode to POST
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost); //set POST content to multi-part form in formpost

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);

		/* Check for errors */
		if (res != CURLE_OK) {
			fprintf(stderr, "- Error while reporting violation: %s\n", curl_easy_strerror(res));
		} else {
			printf("- Violation Reported: %s\n", jsonData.str().c_str());
		}
		
		/* always cleanup */
		curl_easy_cleanup(curl);

		/* then cleanup the formpost chain */
		curl_formfree(formpost);
	}

	

	pthread_exit(NULL);
}