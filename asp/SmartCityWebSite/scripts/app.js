(function () {
    angular.module('app', [
        'ui.router',                // Angular flexible routing
        'angular-flot',             // Flot chart
        'angles',                   // Angular ChartJS
        'ngGrid',                   // Angular ng Grid
        'ngResource',               // AngularJS resource
        'emguo.poller',             // AngularJS Poller
        'uiGmapgoogle-maps'         // AngularJS Maps wrapper
    ])
})();