angular.module('app').factory('TipiSegnalazioniService', function ($resource) {
    return $resource('http://localhost:2188/api/TipiSegnalazioni/:id',
         {}, {
             getCounted: { url: 'http://localhost:2188/api/TipiSegnalazioni/Totali', method: 'GET', isArray: true },
             getMontly: { url: 'http://localhost:2188/api/TipiSegnalazioni/TotaliMensili/:tipoSegnalazione', method: 'GET', params: { tipoSegnlazione: '@tipoSegnalazione' }, isArray: true }
         });
});