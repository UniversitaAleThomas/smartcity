angular.module('app').factory('SegnalazioniService', function ($resource) {
    //Inizializza la resource con l'indirizzo del server e la ritorna
    return $resource('http://localhost:2188/api/Segnalazioni/:id',
     {}, {
         //Crea una get ad un sottoRoute delle api che fornisce l'ultima segnalazione per il tipo passato
         getLast: { url: 'http://localhost:2188/api/Segnalazioni/Last/:tipoSegnalazione', method: 'GET', params: { tipoSegnalazione: '@tipoSegnalazione' }, isArray: false },
     });
});