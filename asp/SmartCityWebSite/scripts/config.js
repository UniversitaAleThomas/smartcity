function configState($stateProvider, $urlRouterProvider, $compileProvider) {
    // Set default state
    $urlRouterProvider.otherwise("/app/home");
    $stateProvider
        // App views
        .state('app', {
            abstract: true,
            url: "/app",
            templateUrl: "views/common/content.html"
        })

        .state('app.home', {
            url: "/home",
            templateUrl: "views/app/home.html",
            data: {
                pageTitle: 'Dashboard',
                pageDesc: 'Offre una visione globale dello stato dei dati.'
            }
        })
        .state('app.grafici', {
            url: "/grafici",
            templateUrl: "views/app/grafici.html",
            data: {
                pageTitle: 'Grafico',
                pageDesc: 'Mostra il numero di segnalazioni ricevute suddivise per mese'
            }
        })
        .state('app.lista', {
            url: "/lista",
            templateUrl: "views/app/lista.html",
            data: {
                pageTitle: 'Elenco',
                pageDesc: 'Visualizzazione ad elenco delle segnalazioni presenti su db ordinate dalla piu\' recente alla piu\' vecchia.'
            }
        })
        .state('app.mappa', {
            url: "/mappa",
            templateUrl: "views/app/mappa.html",
            data: {
                pageTitle: 'Mappa',
                pageDesc: 'Visualizza sulla mappa la posizione delle segnalazioni effettuate dagli utenti.'
            }
        })
}

angular
    .module('app')
    .config(configState)
    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
    });

angular
    .module('app').config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;

        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
    ]);