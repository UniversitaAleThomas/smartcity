angular
    .module('app').config(function (pollerConfig) {
        pollerConfig.neverOverwrite = true;
    })
    .controller('homeCtrl', homeCtrl);

function homeCtrl($scope, TipiSegnalazioniService, SegnalazioniService, poller) {
    $scope.tipi = TipiSegnalazioniService.getCounted();
}