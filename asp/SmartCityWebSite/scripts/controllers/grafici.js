angular
    .module('app')
    .controller('graficiCtrl', graficiCtrl)

function graficiCtrl($scope, poller, TipiSegnalazioniService) {
    /**
     * Data for Sharp Line chart
     */
    $scope.sharpLineData = {
        labels: [],
        datasets: [
            {
                label: "Example dataset",
                fillColor: "rgba(98,203,49,0.5)",
                strokeColor: "rgba(98,203,49,0.7)",
                pointColor: "rgba(98,203,49,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(98,203,49,1)",
                data: [33, 48, 40, 19, 54, 27, 54]
            }
        ]
    };

    /**
     * Options for Sharp Line chart
     */
    $scope.sharpLineOptions = {
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        bezierCurve: false,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 1,
        datasetFill: true,
    };

    var montlyPoller = poller.get(TipiSegnalazioniService, {
        action: 'getMontly',
        delay: 6000
    });

    montlyPoller.promise.then(null, null, function (entries) {
        var newChart = {
            labels: [],
            datasets: [
                {
                    label: "Dataset Mensile",
                    fillColor: "rgba(98,203,49,0.5)",
                    strokeColor: "rgba(98,203,49,0.7)",
                    pointColor: "rgba(98,203,49,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(98,203,49,1)",
                    data: []
                }
            ]
        };
        angular.forEach(entries, function (value, key) {
            newChart.labels.push(value.Id);
            newChart.datasets[0].data.push(value.Count);
        })
        $scope.sharpLineData = newChart;
    });
}