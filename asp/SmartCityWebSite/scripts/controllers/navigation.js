angular
    .module('app')
    .controller('navigationHeaderCtrl', navigationHeaderCtrl)

function navigationHeaderCtrl($scope) {
    $scope.immagineProfilo = "images/thomas.jpg";
    $scope.nome = "Thomas Trapanese";
    var tipo = 0;
    setInterval(function () {
        if (tipo === 0) {
            $scope.immagineProfilo = "images/thomas.jpg";
            $scope.nome = "Thomas Trapanese";
            tipo = 1;
        } else {
            $scope.immagineProfilo = "images/alessandro.jpg";
            $scope.nome = "Alessandro Neri";
            tipo = 0;
        }
    }, 5000);
}