angular
    .module('app').config(function (pollerConfig) {
        pollerConfig.neverOverwrite = true;
    })
    .controller('homeRowCtrl', homeRowCtrl);

function homeRowCtrl($scope, TipiSegnalazioniService, SegnalazioniService, poller) {
    $scope.chartData = [
            {
                label: "bar",
                data: []
            }
    ];
    $scope.chartOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 0.8,
                fill: true,
                fillColor: {
                    colors: [{ opacity: 1 }, { opacity: 1 }]
                },
                lineWidth: 1
            }
        },
        xaxis: {
            tickDecimals: 0
        },
        colors: ["#3498db"],
        grid: {
            show: false
        },
        legend: {
            show: false
        }
    };

    $scope.ultimo = {};

    var ultimoPoller = poller.get(SegnalazioniService, {
        action: 'getLast',
        argumentsArray: [
            {
                tipoSegnalazione: $scope.tipo.Id
            }
        ],
        delay: 60000
    });

    ultimoPoller.promise.then(null, null, function (entries) {
        $scope.ultimo = entries;
        $scope.ultimo.immagine = "http://localhost:2188/api/PhotoFiles/" + $scope.ultimo.Id;

    });

    var montlyPoller = poller.get(TipiSegnalazioniService, {
        action: 'getMontly',
        argumentsArray: [
            {
                tipoSegnalazione: $scope.tipo.Id
            }
        ],
        delay: 6000
    });

    montlyPoller.promise.then(null, null, function (entries) {
        $scope.mensile = entries;
        var newChart = {
        };
        newChart.label = "bar";
        newChart.data = [];
        angular.forEach(entries, function (value, key) {
            newChart.data.push([key, value.Count]);
        })
        $scope.chartData[0].data = newChart.data;

        console.log("montlyPoller: " + JSON.stringify($scope.chartData));
    });
}