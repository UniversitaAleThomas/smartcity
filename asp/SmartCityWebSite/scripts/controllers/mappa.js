angular
    .module('app')
    .controller('mappaCtrl', mappaCtrl)

function mappaCtrl($scope, SegnalazioniService, TipiSegnalazioniService, poller, uiGmapGoogleMapApi, uiGmapIsReady) {
    $scope.selectedValue = { show: false };
    $scope.totale = 0;
    $scope.filtrati = 0;
    var markerInfoOpened = {};
    var markerImage = {
        "1": "images/marker/blue_MarkerB.png",
        "2": "images/marker/brown_MarkerO.png",
        "3": "images/marker/darkgreen_MarkerB.png",
        "4": "images/marker/green_MarkerP.png",
        "5": "images/marker/orange_MarkerC.png",
        "6": "images/marker/paleblue_MarkerV.png",
        "7": "images/marker/red_MarkerV.png",
        "8": "images/marker/yellow_MarkerS.png",
    };

    $scope.selection = {
        ids: {
            "1": true,
            "2": true,
            "3": true,
            "4": true,
            "5": true,
            "6": true,
            "7": true,
            "8": true
        }
    };
    $scope.markers = [];
    // Get poller.
    var myPoller = poller.get(SegnalazioniService, {
        action: 'query',
        delay: 20000
    });

    function filterMarkers() {
        $scope.markers = [];
        angular.forEach($scope.segnalazioniData, function (value, key) {
            if ($scope.selection.ids[value.Tipo]) {
                var nMark = {
                    id: value.Id,
                    tipo: value.Tipo,
                    coords: {
                        latitude: value.Latitudine,
                        longitude: value.Longitudine
                    },
                    image: markerImage[value.Tipo],
                    value: value
                };

                $scope.markers.push(nMark);
            }
        });
        $scope.filtrati = $scope.markers.length;

        console.log($scope.selection)
    }

    $scope.filterChange = filterMarkers;

    myPoller.promise.then(null, null, function (entries) {
        $scope.segnalazioniData = entries;

        $scope.totale = entries.length;
        filterMarkers();
    });




    uiGmapGoogleMapApi.then(function (maps) {
        $scope.map = {
            center: {
                latitude: 44.22,
                longitude: 12.35
            },
            zoom: 14,
            pan: 1,
            control: {},
            events: {
                tilesloaded: function (maps, eventName, args) { },
                dragend: function (maps, eventName, args) { },
                zoom_changed: function (maps, eventName, args) { }
            }
        };

        navigator.geolocation.getCurrentPosition(function (position) {
            $scope.map.center = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            };
        });

        $scope.googlemap = {};

        TipiSegnalazioniService.query().$promise.then(function (data) {
            $scope.tipiSegnalazione = data;
        });
        $scope.openMarkerInfo = function (selectedValue) {
            $scope.selectedValue = selectedValue;
            $scope.selectedValue.descrizione = $scope.tipiSegnalazione[selectedValue.Tipo] ? $scope.tipiSegnalazione[selectedValue.Tipo].Descrizione : "";
            $scope.selectedValue.immagine = "http://localhost:2188/api/PhotoFiles/" + selectedValue.Id;
            $scope.selectedValue.show = true;
            $scope.$apply();
        };
    });
};