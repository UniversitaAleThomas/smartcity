﻿angular
    .module('app')
    .controller('listaCtrl', listaCtrl)

function listaCtrl($scope, $filter, SegnalazioniService, TipiSegnalazioniService, poller) {
    $scope.segnalazioniData = [];
    $scope.grdSelections = [];
    $scope.defaultImage = "/images/no_image.png";
    $scope.selectedImage = "/images/no_image.png";

    // Definisco il poller per gestire il refresh delle segnalazioni.
    var myPoller = poller.get(SegnalazioniService, {
        action: 'query',
        delay: 6000
    });

    //Ottengo i tipi di segnalazioni gestiti dall'applicazione
    TipiSegnalazioniService.query().$promise.then(function (data) {
        $scope.tipi = data;

        //Avvio il polling della richiesta
        myPoller.promise.then(null, null, function (entries) {
            angular.forEach(entries, function (value, key) {
                if ($scope.tipi[value.Tipo])
                    value.Descrizione = $scope.tipi[value.Tipo].Descrizione;
                value.DataF = $filter('date')(value.Data, "dd-MM-yyyy");
                value.OraF = $filter('date')(value.Data, "HH:mm");
            });
            $scope.segnalazioniData = entries;
        });
    });

    $scope.gridOptions = {
        data: 'segnalazioniData',
        columnDefs: [
            { field: 'IdDispositivo', displayName: 'Dispositivo' },
            { field: 'Descrizione', displayName: 'Descrizione' },
            {
                field: 'DataF', displayName: 'Data', sortFn: function (aDate, bDate) {
                    var st = "26.04.2013";
                    var pattern = /(\d{2})\-(\d{2})\-(\d{4})/;

                    //debugger;
                    var a = new Date(aDate.replace(pattern, '$3-$2-$1'));
                    var b = new Date(bDate.replace(pattern, '$3-$2-$1'));
                    if (a < b) {
                        return -1;
                    }
                    else if (a > b) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            },
            { field: 'OraF', displayName: 'Ora' }
        ],
        multiSelect: false,
        selectedItems: $scope.grdSelections,
        afterSelectionChange: function (data) {
            if (data.selected == true) {
                $scope.selectedImage = "http://localhost:2188/api/PhotoFiles/" + $scope.grdSelections[0].Id;
            } else {
                $scope.selectedImage = "/images/no_image.png";
            }
        },
    };
}