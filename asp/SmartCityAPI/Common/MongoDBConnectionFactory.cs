﻿using MongoDB.Driver;

namespace SmartCityAPI.Common
{
    public class MongoDbConnectionFactory
    {
        private static MongoServer _server;
        private static MongoDatabase _database;
        private static bool _connected;

        public static MongoDatabase GetScDatabase()
        {
            if (_database != null) return _database;
            if (!_connected) Connect();
            _database = _server.GetDatabase("smartCity");
            return _database;
        }

        public static MongoServer GetConnection()
        {
            if (!_connected)
            {
                Connect();
            }
            return _server;
        }

        public static void DestroyConnection()
        {
            if (_connected)
            {
                _database = null;
                Disconnect();
            }
        }

        private static void Connect()
        {
            var connectionString = "mongodb://localhost";
            var cl = new MongoClient(connectionString);
            _server = cl.GetServer();
            _server.Connect();
            _connected = true;
        }

        private static void Disconnect()
        {
            _server.Disconnect();
            _connected = false;
        }
    }
}