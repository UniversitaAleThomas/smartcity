﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace SmartCityAPI.Models
{
    public class Segnalazione
    {
        [BsonElement("_id")]
        public string Id { get; set; }

        [BsonElement("tipo")]
        public int Tipo { get; set; }

        [BsonElement("idDispositivo")]
        public string IdDispositivo { get; set; }

        [BsonElement("data")]
        public DateTime Data { get; set; }

        [BsonElement("latitudine")]
        public double Latitudine { get; set; }

        [BsonElement("longitudine")]
        public double Longitudine { get; set; }
    }
}