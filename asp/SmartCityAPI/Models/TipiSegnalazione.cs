﻿using MongoDB.Bson.Serialization.Attributes;

namespace SmartCityAPI.Models
{
    public class TipiSegnalazione
    {
        [BsonElement("_id")]
        public int Id { get; set; }

        [BsonElement("descrizione")]
        public string Descrizione { get; set; }

        [BsonElement("manuale")]
        public bool Manuale { get; set; }
    }
}