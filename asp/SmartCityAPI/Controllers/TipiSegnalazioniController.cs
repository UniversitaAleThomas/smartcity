﻿using MongoDB.Driver.Linq;
using SmartCityAPI.Common;
using SmartCityAPI.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace SmartCityAPI.Controllers
{
    [RoutePrefix("api/TipiSegnalazioni")]
    public class TipiSegnalazioniController : ApiController
    {
        [Route("Totali")]
        // GET: api/TipiSegnalazioni/Totali
        public IQueryable<object> GetTotali()
        {
            var database = MongoDbConnectionFactory.GetScDatabase();
            var tipiCollection = database.GetCollection<TipiSegnalazione>("tipiSegnalazioni");
            var segnalazioniCollection = database.GetCollection<Segnalazione>("segnalazioni");

            IQueryable<object> matches = (from tipo in tipiCollection.AsQueryable().ToList<TipiSegnalazione>()
                                          join seg in segnalazioniCollection.AsQueryable()
                                              on tipo.Id equals seg.Tipo
                                          group new { tipo, seg.Data } by new { tipo }
                                              into g
                                              select new
                                              {
                                                  g.Key.tipo.Id,
                                                  g.Key.tipo.Descrizione,
                                                  g.Key.tipo.Manuale,
                                                  Ultimo = g.Max(x => x.Data),
                                                  Count = g.Count()
                                              }).AsQueryable();
            return matches;
        }

        [Route("TotaliMensili/{tipoSegnalazione}")]
        // GET: api/TipiSegnalazioni/TotaliMensili/5
        /// <summary>
        /// Richimato per popolare i grafici nella dashboard
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        public IQueryable<object> GetTotaliMensili(int tipoSegnalazione)
        {
            var database = MongoDbConnectionFactory.GetScDatabase();
            var tipiCollection = database.GetCollection<TipiSegnalazione>("tipiSegnalazioni");
            var segnalazioniCollection = database.GetCollection<Segnalazione>("segnalazioni");

            IQueryable<object> matches = (from tipo in tipiCollection.AsQueryable().ToList<TipiSegnalazione>()
                                          join seg in segnalazioniCollection.AsQueryable()
                                              on tipo.Id equals seg.Tipo
                                          where tipo.Id == tipoSegnalazione
                                          group new { seg.Data.Month, seg.Data.Year }
                                              by new { seg.Data.Month, seg.Data.Year }
                                              into g
                                              select new
                                              {
                                                  Id = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(g.Key.Month) + "-" + g.Key.Year,
                                                  Order = g.Key.Year + "-" + g.Key.Month,
                                                  Count = g.Count()
                                              }).OrderBy(x => x.Order).AsQueryable();
            return matches;
        }

        // GET: api/TipiSegnalazioni/TotaliMensili
        /// <summary>
        /// Richimato per popolare la pagina dei grafici
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        [Route("TotaliMensili")]
        public IQueryable<object> GetTotaliMensili()
        {
            var database = MongoDbConnectionFactory.GetScDatabase();
            var tipiCollection = database.GetCollection<TipiSegnalazione>("tipiSegnalazioni");
            var segnalazioniCollection = database.GetCollection<Segnalazione>("segnalazioni");

            IQueryable<object> matches = (from tipo in tipiCollection.AsQueryable().ToList<TipiSegnalazione>()
                                          join seg in segnalazioniCollection.AsQueryable()
                                              on tipo.Id equals seg.Tipo
                                          where seg.Data.Year == DateTime.Now.Year
                                          group new { seg.Data.Month, seg.Data.Year }
                                              by new { seg.Data.Month, seg.Data.Year }
                                              into g
                                              select new
                                              {
                                                  Id = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(g.Key.Month) + "-" + g.Key.Year,
                                                  Order = g.Key.Year + "-" + g.Key.Month,
                                                  Count = g.Count()
                                              }).OrderBy(x => x.Order).AsQueryable();
            return matches;
        }

        // GET: api/TipiSegnalazioni
        public IQueryable<object> GetTipiSegnalazioni()
        {
            var database = MongoDbConnectionFactory.GetScDatabase();
            var tipiCollection = database.GetCollection<TipiSegnalazione>("tipiSegnalazioni");
            return tipiCollection.AsQueryable();
        }

        // GET: api/TipiSegnalazioni/5
        [ResponseType(typeof(TipiSegnalazione))]
        public IHttpActionResult GetTipiSegnalazione(int id)
        {
            var database = MongoDbConnectionFactory.GetScDatabase();
            var collection = database.GetCollection<TipiSegnalazione>("tipiSegnalazioni");

            // Search documents
            var query = from entity in collection.AsQueryable()
                        where (entity.Id == id)
                        select entity;

            var tipiSegnalazione = query.Single();

            if (tipiSegnalazione == null)
            {
                return NotFound();
            }

            return Ok(tipiSegnalazione);
        }
    }
}