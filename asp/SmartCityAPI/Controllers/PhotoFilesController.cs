﻿using MongoDB.Bson;
using Newtonsoft.Json;
using SmartCityAPI.Common;
using SmartCityAPI.Models;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;

namespace SmartCityAPI.Controllers
{
    public class PhotoFilesController : ApiController
    {
        private const string FOLDER_UPLOAD = "uploads";
        private const string FOLDER_IMAGE = "image";

        public HttpResponseMessage GetImmagine(string id)
        {
            HttpResponseMessage result;

            DirectoryInfo directoryInfo = new DirectoryInfo(HostingEnvironment.MapPath("~/App_Data/" + FOLDER_IMAGE));
            FileInfo foundFileInfo = directoryInfo.GetFiles().Where(x => x.Name == id + ".thumb").FirstOrDefault();
            if (foundFileInfo == null)
            {
                var fs = new FileStream(HostingEnvironment.MapPath("~/App_Data/no_image.png"), FileMode.Open,
                    FileAccess.Read);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StreamContent(fs);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            }
            else
            {
                var fs = new FileStream(foundFileInfo.FullName, FileMode.Open, FileAccess.Read);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StreamContent(fs);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = foundFileInfo.Name
                };
            }

            return result;
        }

        public Task<HttpResponseMessage> PostImmagine()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            string imgPath = HostingEnvironment.MapPath("~/App_Data/" + FOLDER_IMAGE);
            string root = HostingEnvironment.MapPath("~/App_Data/" + FOLDER_UPLOAD);
            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task.
            var task = Request.Content.ReadAsMultipartAsync(provider).
                ContinueWith(t =>
                {
                    if (t.IsFaulted || t.IsCanceled)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                    }
                    //Leggo la segnalazione e la salvo
                    var seg = JsonConvert.DeserializeObject<Segnalazione>(provider.FormData.Get(0));

                    var database = MongoDbConnectionFactory.GetScDatabase();
                    var collection = database.GetCollection<Segnalazione>("segnalazioni");
                    seg.Id = ObjectId.GenerateNewId().ToString();
                    collection.Insert(seg);

                    //Salvo l'immagine con l'id della segnalazione
                    foreach (var file in provider.FileData)
                    {
                        File.Move(file.LocalFileName, imgPath + "/" + seg.Id + ".jpg");
                        var image = Image.FromFile(imgPath + "/" + seg.Id + ".jpg");
                        var ratio = image.Width / (float)image.Height;
                        var thumb = image.GetThumbnailImage(240, (int)(240 / ratio), () => false, IntPtr.Zero);
                        thumb.Save(Path.ChangeExtension(imgPath + "/" + seg.Id + ".jpg", "thumb"));
                    }

                    return Request.CreateResponse(HttpStatusCode.OK);
                });

            return task;
        }
    }
}