﻿using MongoDB.Bson;
using MongoDB.Driver.Linq;
using SmartCityAPI.Common;
using SmartCityAPI.Models;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace SmartCityAPI.Controllers
{
    [RoutePrefix("api/Segnalazioni")]
    public class SegnalazioniController : ApiController
    {
        // GET: api/Segnalazioni
        public IQueryable<Segnalazione> GetSegnalazioni()
        {
            var database = MongoDbConnectionFactory.GetScDatabase();
            var collection = database.GetCollection<Segnalazione>("segnalazioni");

            return collection.AsQueryable();
        }

        // GET: api/Segnalazioni/5
        public IHttpActionResult GetSegnalazione(string id)
        {
            var database = MongoDbConnectionFactory.GetScDatabase();
            var collection = database.GetCollection<Segnalazione>("segnalazioni");

            // Search documents
            var query = from entity in collection.AsQueryable()
                        where (entity.Id == id)
                        select entity;

            var segnalazione = query.Single();

            if (segnalazione == null)
            {
                return NotFound();
            }

            return Ok(segnalazione);
        }

        // POST: api/Segnalazioni
        public IHttpActionResult PostSegnalazione(Segnalazione segnalazione)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var database = MongoDbConnectionFactory.GetScDatabase();
            var collection = database.GetCollection<Segnalazione>("segnalazioni");
            segnalazione.Id = ObjectId.GenerateNewId().ToString();
            // Insert the new document into the collection
            collection.Insert(segnalazione);

            return Ok(segnalazione);
        }

        // GET: api/Segnalazioni/Last/5
        [Route("Last/{tipoSegnalazione}")]
        public IHttpActionResult GetLast(int tipoSegnalazione)
        {
            var database = MongoDbConnectionFactory.GetScDatabase();
            var collection = database.GetCollection<Segnalazione>("segnalazioni");

            var segnalazione = (from s in collection.AsQueryable()
                                where s.Tipo == tipoSegnalazione
                                select s).OrderByDescending(x => x.Data).First();

            if (segnalazione == null)
            {
                return NotFound();
            }

            return Ok(segnalazione);
        }
    }
}