package mobile.smartcity.uni.smartcity.rest.model;

import java.util.Date;

/**
 * Created by thomas on 6/13/15.
 */
public class Segnalazione {
    private String Id;
    private int Tipo;
    private String IdDispositivo;
    private double Latitudine, Longitudine;
    private Date Data;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getTipo() {
        return Tipo;
    }

    public void setTipo(int tipo) {
        Tipo = tipo;
    }

    public String getIdDispositivo() {
        return IdDispositivo;
    }

    public void setIdDispositivo(String idDispositivo) {
        IdDispositivo = idDispositivo;
    }

    public double getLatitudine() {
        return Latitudine;
    }

    public void setLatitudine(double latitudine) {
        Latitudine = latitudine;
    }

    public double getLongitudine() {
        return Longitudine;
    }

    public void setLongitudine(double longitudine) {
        Longitudine = longitudine;
    }

    public Date getData() {
        return Data;
    }

    public void setData(Date data) {
        Data = data;
    }
}
