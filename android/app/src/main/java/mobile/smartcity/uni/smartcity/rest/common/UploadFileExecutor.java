package mobile.smartcity.uni.smartcity.rest.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.File;

import mobile.smartcity.uni.smartcity.rest.model.Segnalazione;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class UploadFileExecutor extends AsyncTask<File, Integer, Boolean> {
    private static final String TAG = "Uploadfile";
    private Context context;
    private SmartCityRestService smartCityRestService;
    private Segnalazione segnalazione;
    private ProgressDialog progressDialog;
    private OnCompleteListener onCompleteListener;

    public UploadFileExecutor(Context context, SmartCityRestService smartCityRestService, Segnalazione segnalazione, OnCompleteListener onCompleteListener) {
        this.context = context;
        this.onCompleteListener = onCompleteListener;
        this.smartCityRestService = smartCityRestService;
        this.segnalazione = segnalazione;
        progressDialog = new ProgressDialog(context);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progressDialog.setMessage("Caricamento in corso: " + values[0] + "%");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        //processing the progress dialog...
        progressDialog.setMessage("Caricamento in corso");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(File... params) {
        final File file = params[0];
        TypedFile typedFile = new CountingTypedFile("image/jpeg", file, new ProgressListener() {
            @Override
            public void transferred(long num) {
                publishProgress((int) ((num / (float) file.length()) * 100));
            }
        });

        try {
            String segStr = new Gson().toJson(segnalazione);
            smartCityRestService.aggiungiSegnalazione(new TypedString(segStr), typedFile);
            return true;
        } catch (Exception ex) {
            Log.e(TAG, "Error on sending items..", ex);
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (progressDialog.isShowing())
            progressDialog.dismiss();

        //check if result is not null, and send it..
        if (result == null || Boolean.FALSE.equals(result)) {
            onCompleteListener.onFailure();
            return;
        }

        onCompleteListener.onSuccess();
        Log.i(TAG, "Successfully uploaded the file...");
    }

    public interface OnCompleteListener {
        void onSuccess();
        void onFailure();
    }
}



