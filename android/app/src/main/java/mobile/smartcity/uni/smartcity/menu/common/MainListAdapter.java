package mobile.smartcity.uni.smartcity.menu.common;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mobile.smartcity.uni.smartcity.R;

/**
 * Created by thomas on 5/18/15.
 */
public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.MainListViewHolder> {
    private List<MenuElement> list;
    private Context ctx;
    private OnMenuSelection onMenuSelection;

    public MainListAdapter(Context ctx, List<MenuElement> list, OnMenuSelection onMenuSelection) {
        super();
        this.list = list;
        this.ctx = ctx;
        this.onMenuSelection = onMenuSelection;
    }


    @Override
    public MainListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.fragment_main_list_row, parent, false);
        return new MainListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainListViewHolder holder, int position) {
        MenuElement item = list.get(position);
        holder.bindData(item);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MainListViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private ImageView imgView;
        private CardView cardView;

        public MainListViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.item_title);
            imgView = (ImageView) itemView.findViewById(R.id.item_image);
            cardView = (CardView) itemView.findViewById(R.id.item_card);
        }

        public void bindData(final MenuElement element) {
            tvTitle.setText(element.getTitle());
            imgView.setImageDrawable(element.getDrawable());
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onMenuSelection != null){
                        onMenuSelection.onItemSelected(element);
                    }
                }
            });
        }
    }

    public interface OnMenuSelection {
        void onItemSelected(MenuElement item);
    }
}
