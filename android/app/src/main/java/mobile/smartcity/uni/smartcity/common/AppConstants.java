package mobile.smartcity.uni.smartcity.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;

/**
 * Created by thomas on 5/25/15.
 */
public class AppConstants {
    public static final String APP_NAME = "SmartCity";

    public static final String PARAM_TIPO_SEGNALAZIONE = "tipoSegnalazione";
    public static final String PARAM_SEGNALAZIONE = "segnalazione";
    public static final String PARAM_TIPO_DATA = "data";
    public static final String PARAM_TIPO_LONGITUDINE = "longitudine";
    public static final String PARAM_TIPO_LATITUDINE = "latitudine";

    public static final String PREF_INDIRIZZO_IP = "serverIp";
    public static final String PREF_PORTA = "serverPort";

    public static final SimpleDateFormat SDF_DATA = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat SDF_ORA = new SimpleDateFormat("HH:mm");
    public static final String PARAM_ID_TIPO_SEGNALAZIONE = "idTipoSegnalazione";

    public static String getWSEndPoint(Context ctx) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        StringBuilder ret = new StringBuilder();
        ret.append("http://");
        ret.append(prefs.getString(PREF_INDIRIZZO_IP, ""));
        ret.append(":");
        ret.append(prefs.getString(PREF_PORTA, ""));
        return ret.toString();
    }
}
