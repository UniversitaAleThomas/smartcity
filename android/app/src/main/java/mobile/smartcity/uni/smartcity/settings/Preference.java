package mobile.smartcity.uni.smartcity.settings;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import mobile.smartcity.uni.smartcity.R;

public class Preference extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference_headers);
    }
}