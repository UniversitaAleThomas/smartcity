package mobile.smartcity.uni.smartcity.dettagli;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import mobile.smartcity.uni.smartcity.R;
import mobile.smartcity.uni.smartcity.common.AppConstants;
import mobile.smartcity.uni.smartcity.common.GPSActivity;


public class DettagliActivity extends GPSActivity {

    private DettagliFragment fr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar actionBar = (Toolbar) findViewById(R.id.action_bar);
        setSupportActionBar(actionBar);
        int idTipoSegnalazione = getIntent().getIntExtra(AppConstants.PARAM_ID_TIPO_SEGNALAZIONE, 0);
        String tipoSegnalazione = getIntent().getStringExtra(AppConstants.PARAM_TIPO_SEGNALAZIONE);
        fr = DettagliFragment.newInstance(idTipoSegnalazione, tipoSegnalazione);
        FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
        Fragment oldFr = getSupportFragmentManager().findFragmentById(R.id.container);
        if (oldFr != null) tr.remove(oldFr);
        tr.add(R.id.container, fr);
        tr.commit();
    }

    @Override
    protected void onUpdateLocation() {
        if (fr != null)
            fr.updateLocation(getLocation());
    }
}
