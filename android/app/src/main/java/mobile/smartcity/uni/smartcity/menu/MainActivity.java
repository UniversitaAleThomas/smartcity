package mobile.smartcity.uni.smartcity.menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import mobile.smartcity.uni.smartcity.R;
import mobile.smartcity.uni.smartcity.common.SuperActivity;


public class MainActivity extends SuperActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        Fragment fr = MainListFragment.newInstance();

        FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
        Fragment oldFr = getSupportFragmentManager().findFragmentById(R.id.container);
        if (oldFr != null) tr.remove(oldFr);
        tr.add(R.id.container, fr);
        tr.commit();
    }

}
