package mobile.smartcity.uni.smartcity.rest.common;

public interface ProgressListener {
    void transferred(long num);
}