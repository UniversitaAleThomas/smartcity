package mobile.smartcity.uni.smartcity.rest.common;

import java.util.List;

import mobile.smartcity.uni.smartcity.rest.model.Segnalazione;
import mobile.smartcity.uni.smartcity.rest.model.TipiSegnalazione;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by thomas on 6/13/15.
 */
public interface SmartCityRestService {


    @GET("/api/TipiSegnalazioni")
    void getTipiSegnalazioni(Callback<List<TipiSegnalazione>> cb);
    @GET("/api/TipiSegnalazioni/{id}")
    void getTipoSegnalazione(@Path("id") int id, Callback<TipiSegnalazione> cb);

    @GET("/api/Segnalazioni")
    void getSegnalazioni(Callback<List<Segnalazione>> cb);

    @Multipart
    @POST("/api/PhotoFiles")
    String aggiungiSegnalazione(@Part("Segnalazione") TypedString segnalazione, @Part("file") TypedFile file);

    @GET("/api/PhotoFiles/{id}")
    void getFoto(@Path("id") String id, Callback<Response> cb );
}
