package mobile.smartcity.uni.smartcity.menu.common;

import android.graphics.drawable.Drawable;

public class MenuElement {
    private int id;
    private Drawable drawable;
    private String title;

    public MenuElement(int id, String title, Drawable drawable) {
        this.id = id;
        this.title = title;
        this.drawable = drawable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
