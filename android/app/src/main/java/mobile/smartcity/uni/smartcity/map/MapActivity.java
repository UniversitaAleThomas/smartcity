package mobile.smartcity.uni.smartcity.map;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import mobile.smartcity.uni.smartcity.R;
import mobile.smartcity.uni.smartcity.common.GPSActivity;
import mobile.smartcity.uni.smartcity.rest.RESTUtil;
import mobile.smartcity.uni.smartcity.rest.model.Segnalazione;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MapActivity extends GPSActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private GoogleMap mMap;
    private List<Segnalazione> segnalazioni;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        init();

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        RESTUtil.getSmartCityRestService(this).getSegnalazioni(new Callback<List<Segnalazione>>() {
            @Override
            public void success(List<Segnalazione> segnalaziones, Response response) {
                segnalazioni = segnalaziones;
                updatePOI();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.i("segnalazioni", error.getMessage());
            }
        });
    }

    public void updatePOI() {
        if (mMap != null) {
            for (Segnalazione s : segnalazioni) {
                mMap.addMarker(
                        new MarkerOptions().position(
                                new LatLng(s.getLatitudine(), s.getLongitudine())
                        ).title(s.getTipo() + ": " + s.getData()));
            }
        }
    }

    @Override
    protected void onUpdateLocation() {
        Location mLastLocation = getLocation();

        if (mLastLocation != null && mMap != null) {
            mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                            new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()
                            )
                            , 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.setOnMarkerClickListener(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        onUpdateLocation();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.i("log_marker",marker.getId());
        MarkerDialog.newInstance(segnalazioni.get(Integer.parseInt(marker.getId().substring(1)))).show(getFragmentManager(), "markerDialog");
        return false;
    }
}