package mobile.smartcity.uni.smartcity.rest.model;

/**
 * Created by thomas on 6/13/15.
 */
public class TipiSegnalazione {
    private int Id;
    private String Descrizione;
    private boolean Manuale;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDescrizione() {
        return Descrizione;
    }

    public void setDescrizione(String descrizione) {
        Descrizione = descrizione;
    }

    public boolean isManuale() {
        return Manuale;
    }

    public void setManuale(boolean manuale) {
        Manuale = manuale;
    }
}
