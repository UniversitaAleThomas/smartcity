package mobile.smartcity.uni.smartcity.dettagli;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import mobile.smartcity.uni.smartcity.R;
import mobile.smartcity.uni.smartcity.common.AppConstants;
import mobile.smartcity.uni.smartcity.rest.RESTUtil;
import mobile.smartcity.uni.smartcity.rest.common.UploadFileExecutor;
import mobile.smartcity.uni.smartcity.rest.model.Segnalazione;


public class DettagliFragment extends DialogFragment {
    //Dati
    private int idTipoSegnalazione;
    private String tipoSegnalazione;
    private Date date;
    private double lat, lng;

    //View
    private ImageView imgPhoto;
    private TextView tvGPSLat;
    private TextView tvGPSLon;

    public static DettagliFragment newInstance(int idTipoSegnalazione, String tipoSegnalazione) {
        DettagliFragment fragment = new DettagliFragment();
        Bundle args = new Bundle();
        args.putInt(AppConstants.PARAM_ID_TIPO_SEGNALAZIONE, idTipoSegnalazione);
        args.putString(AppConstants.PARAM_TIPO_SEGNALAZIONE, tipoSegnalazione);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        idTipoSegnalazione = getArguments().getInt(AppConstants.PARAM_ID_TIPO_SEGNALAZIONE);
        tipoSegnalazione = getArguments().getString(AppConstants.PARAM_TIPO_SEGNALAZIONE);

        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE); // si usa solo per la dialog

        imgPhoto = (ImageView) view.findViewById(R.id.imgPhoto);
        TextView tvData = (TextView) view.findViewById(R.id.tvData);
        TextView tvOra = (TextView) view.findViewById(R.id.tvOra);
        tvGPSLon = (TextView) view.findViewById(R.id.tvGPSLon);
        tvGPSLat = (TextView) view.findViewById(R.id.tvGPSLat);
        TextView tvTipoSegnalazione = (TextView) view.findViewById(R.id.tvTipoSegnalazione);

        if (savedInstanceState != null) {

            lat = savedInstanceState.getDouble(AppConstants.PARAM_TIPO_LATITUDINE);
            lng = savedInstanceState.getDouble(AppConstants.PARAM_TIPO_LONGITUDINE);
            long longDate = savedInstanceState.getLong(AppConstants.PARAM_TIPO_DATA);
            date = new Date(longDate);
        } else {
            date = new Date();
        }

        tvData.setText(AppConstants.SDF_DATA.format(date));
        tvOra.setText(AppConstants.SDF_ORA.format(date));
        tvTipoSegnalazione.setText(tipoSegnalazione);


        // Initialize the location fields
        tvGPSLon.setText("Rilevamento in corso");
        tvGPSLat.setText("Rilevamento in corso");

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.detail, menu);
    }

    public void updateLocation(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();

        tvGPSLon.setText("" + location.getLongitude());
        tvGPSLat.setText("" + location.getLatitude());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_gallery:
                dispatchChoosePictureIntent();
                break;
            case R.id.action_camera:
                dispatchTakePictureIntent();
                break;
            case R.id.action_send:
                String android_id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                Segnalazione segnalazione = new Segnalazione();
                segnalazione.setTipo(idTipoSegnalazione);
                segnalazione.setIdDispositivo(android_id);
                segnalazione.setData(new Date());

                segnalazione.setLatitudine(lat);
                segnalazione.setLongitudine(lng);

                File tmp = new File(Environment.getExternalStorageDirectory() + "/sc_tmp.jpg");

                UploadFileExecutor executor = new UploadFileExecutor(getActivity(), RESTUtil.getSmartCityRestService(getActivity()), segnalazione, new UploadFileExecutor.OnCompleteListener() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(getActivity(), "La segnalazione è stata inviata correttamente", Toast.LENGTH_LONG).show();
                        getActivity().finish();
                    }

                    @Override
                    public void onFailure() {
                        Toast.makeText(getActivity(), "Errore durante l'upload dell'immagine", Toast.LENGTH_LONG).show();
                    }
                });
                executor.execute(tmp);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(AppConstants.PARAM_TIPO_LATITUDINE, lat);
        outState.putDouble(AppConstants.PARAM_TIPO_LONGITUDINE, lng);
        outState.putLong(AppConstants.PARAM_TIPO_DATA, date.getTime());
    }

    static final int REQUEST_IMAGE_CAPTURE = 1234;
    static final int REQUEST_IMAGE_CHOOSE = 653;

    private void dispatchChoosePictureIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQUEST_IMAGE_CHOOSE);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File tmp = new File(Environment.getExternalStorageDirectory() + "/sc_tmp.jpg");
        if (tmp.exists()) tmp.delete();
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tmp));
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            try {
                File tmp = new File(Environment.getExternalStorageDirectory() + "/sc_tmp.jpg");
                imgPhoto.setImageURI(Uri.fromFile(tmp));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_IMAGE_CHOOSE && resultCode == Activity.RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap photo = BitmapFactory.decodeFile(picturePath);

            try {
                File tmp = new File(Environment.getExternalStorageDirectory() + "/sc_tmp.jpg");
                if (tmp.exists()) tmp.delete();
                FileOutputStream out = new FileOutputStream(tmp);
                photo.compress(Bitmap.CompressFormat.JPEG, 90, out);

                imgPhoto.setImageBitmap(photo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
