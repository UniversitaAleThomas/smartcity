package mobile.smartcity.uni.smartcity.map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import mobile.smartcity.uni.smartcity.R;
import mobile.smartcity.uni.smartcity.common.AppConstants;
import mobile.smartcity.uni.smartcity.rest.RESTUtil;
import mobile.smartcity.uni.smartcity.rest.model.Segnalazione;
import mobile.smartcity.uni.smartcity.rest.model.TipiSegnalazione;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class MarkerDialog extends DialogFragment {
    private Segnalazione seg;

    public static MarkerDialog newInstance(Segnalazione seg) {
        MarkerDialog fragment = new MarkerDialog();
        Bundle args = new Bundle();
        Gson gson = new Gson();
        args.putString(AppConstants.PARAM_SEGNALAZIONE, gson.toJson(seg));
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_detail, null);

        Gson gson = new Gson();
        String strSeg = getArguments().getString(AppConstants.PARAM_SEGNALAZIONE);
        seg = gson.fromJson(strSeg, Segnalazione.class);

        final ImageView imgPhoto = (ImageView) view.findViewById(R.id.imgPhoto);
        TextView tvData = (TextView) view.findViewById(R.id.tvData);
        TextView tvOra = (TextView) view.findViewById(R.id.tvOra);
        TextView tvGPSLon = (TextView) view.findViewById(R.id.tvGPSLon);
        TextView tvGPSLat = (TextView) view.findViewById(R.id.tvGPSLat);
        final TextView tvTipoSegnalazione = (TextView) view.findViewById(R.id.tvTipoSegnalazione);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        tvData.setText(AppConstants.SDF_DATA.format(seg.getData()));
        tvOra.setText(AppConstants.SDF_ORA.format(seg.getData()));
        tvGPSLon.setText(String.valueOf(seg.getLongitudine()));
        tvGPSLat.setText(String.valueOf(seg.getLatitudine()));

        RESTUtil.getSmartCityRestService(getActivity())
                .getTipoSegnalazione(seg.getTipo(), new Callback<TipiSegnalazione>() {
                    @Override
                    public void success(TipiSegnalazione tipiSegnalazione, Response response) {
                        tvTipoSegnalazione.setText(tipiSegnalazione.getDescrizione());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getActivity(), "Impossibile recuperare la descrizione della segnalazione", Toast.LENGTH_SHORT).show();
                    }
                });

        try {
            RESTUtil.getSmartCityRestService(getActivity())
                    .getFoto(seg.getId(), new Callback<Response>() {
                        @Override
                        public void success(Response response, Response response2) {
                            if (response.getStatus() != 200) {
                                Toast.makeText(getActivity(), response.getReason(), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                return;
                            }
                            byte[] byteArray = ((TypedByteArray) response.getBody()).getBytes();
                            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                            imgPhoto.setImageBitmap(bmp);
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Impossibile recuperare l'immagine", Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {
            Log.e("MarkerDialog", "Errore nel caricamenteo dell'immagine", e);
        }


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        dialogBuilder.setView(view);
        return dialogBuilder.create();
    }
}