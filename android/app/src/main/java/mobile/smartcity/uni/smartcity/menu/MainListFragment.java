package mobile.smartcity.uni.smartcity.menu;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mobile.smartcity.uni.smartcity.R;
import mobile.smartcity.uni.smartcity.common.AppConstants;
import mobile.smartcity.uni.smartcity.dettagli.DettagliActivity;
import mobile.smartcity.uni.smartcity.menu.common.MainListAdapter;
import mobile.smartcity.uni.smartcity.menu.common.MenuElement;
import mobile.smartcity.uni.smartcity.rest.RESTUtil;
import mobile.smartcity.uni.smartcity.rest.model.TipiSegnalazione;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainListFragment extends Fragment implements Callback<List<TipiSegnalazione>> {
    private RecyclerView recList;
    private List<MenuElement> adapter = new ArrayList<>();
    private List<Drawable> colori = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_main_list, container, false);
        recList = (RecyclerView) view.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_blue_light)));
        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_orange_light)));
        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_green_light)));
        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_red_light)));
        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_purple)));
        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_blue_bright)));
        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_orange_dark)));
        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_green_dark)));
        colori.add(new ColorDrawable(getResources().getColor(android.R.color.holo_red_dark)));

        RESTUtil.getSmartCityRestService(getActivity()).getTipiSegnalazioni(this);

        return view;
    }

    public static MainListFragment newInstance() {
        return new MainListFragment();
    }

    @Override
    public void success(List<TipiSegnalazione> tipiSegnalazioni, Response response) {
        for (int i = 0; i < tipiSegnalazioni.size(); i++) {
            TipiSegnalazione t = tipiSegnalazioni.get(i);
            if (t.isManuale())
                adapter.add(new MenuElement(t.getId(), t.getDescrizione(), colori.get(i % colori.size())));
        }

        recList.setAdapter(new MainListAdapter(getActivity(), adapter, new MainListAdapter.OnMenuSelection() {
            @Override
            public void onItemSelected(MenuElement item) {
                Intent takeDetailIntent = new Intent(getActivity(), DettagliActivity.class);
                takeDetailIntent.putExtra(AppConstants.PARAM_ID_TIPO_SEGNALAZIONE, item.getId());
                takeDetailIntent.putExtra(AppConstants.PARAM_TIPO_SEGNALAZIONE, item.getTitle());
                startActivity(takeDetailIntent);
            }
        }));
    }

    @Override
    public void failure(RetrofitError error) {
        Log.e("RemoteRequest", error.getMessage());
    }
}

