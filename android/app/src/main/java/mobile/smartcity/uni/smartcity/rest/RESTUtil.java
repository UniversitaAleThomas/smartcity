package mobile.smartcity.uni.smartcity.rest;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mobile.smartcity.uni.smartcity.common.AppConstants;
import mobile.smartcity.uni.smartcity.rest.common.SmartCityRestService;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by thomas on 5/25/15.
 */
public class RESTUtil {
    private static RestAdapter getRestAdapter(Context ctx) {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
        return new RestAdapter.Builder()
                .setEndpoint(AppConstants.getWSEndPoint(ctx))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build();
    }

    public static SmartCityRestService getSmartCityRestService(Context ctx) {
        RestAdapter restAdapter = getRestAdapter(ctx);
        return restAdapter.create(SmartCityRestService.class);

    }
}
